package main

import (
	"fmt"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/godevtools-pkg/metrics"
	_ "gitlab.com/godevtools-pkg/metrics"
	"log"
	"net/http"
)

func pingHandler(w http.ResponseWriter, _ *http.Request) {
	metrics.NumPings.Inc()
	w.Write([]byte("pong!\n"))
}

func main() {
	fmt.Println("Simple example")

	http.Handle("/metrics", promhttp.Handler())
	http.Handle("/ping", http.HandlerFunc(pingHandler))
	log.Fatal(http.ListenAndServe(":8090", nil))
}
