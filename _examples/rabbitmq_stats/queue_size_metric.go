package main

import (
	"context"
	"fmt"
	"github.com/streadway/amqp"
	"gitlab.com/godevtools-pkg/metrics"
	_ "gitlab.com/godevtools-pkg/metrics"
	"log"
	"math/rand"
	"time"
)

const (
	brokerUrl = "amqp://guest:guest@rabbitmq:5672"
	queueName = "testQueue"
)

func main() {
	fmt.Println("Queue size tracking example")

	conn, err := amqp.Dial(brokerUrl)
	if err != nil {
		log.Fatal(err)
	}

	channel, err := conn.Channel()
	if err != nil {
		log.Fatal(err)
	}

	_, err = channel.QueueDeclare(queueName, true, false, false, false, nil)
	if err != nil {
		log.Fatal(err)
	}

	publishTicker := time.NewTicker(2 * time.Second)
	consumeTicker := time.NewTicker(15 * time.Second)
	go func() {
		for {
			select {
			case <-publishTicker.C:
				for i := 0; i <= rand.Intn(10); i++ {
					if err = channel.Publish(
						"",        // publish to an exchange
						queueName, // routing to 0 or more queues
						false,     // mandatory
						false,     // immediate
						amqp.Publishing{
							ContentType:  "text/plain",
							Body:         []byte("test_message"),
							DeliveryMode: amqp.Persistent, // 1=non-persistent, 2=persistent
							Priority:     0,               // 0-9
						},
					); err != nil {
						log.Fatal(err)
					}
				}
			}
		}
	}()
	go func() {
		for {
			select {
			case <-consumeTicker.C:
				deliveryChannel, err := channel.Consume(queueName, "", false, false, false, false, nil)
				if err != nil {
					log.Panic(err)
				}
				for msg := range deliveryChannel {
					fmt.Println("Message: ", string(msg.Body))
					msg.Ack(false)
				}
			}
		}
	}()

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// the function QueueSizeTracking takes as a parameter a channel created using
	// the "github.com/streadway/amqp" library (streadwayCh)
	// or using the "github.com/rabbitmq/amqp091-go" library (amqpGoCh)
	go func() {
		err = metrics.QueueSizeTracking(ctx, queueName, nil, channel, 10*time.Second)
		if err != nil {
			log.Panic(err)
		}
	}()

	err = metrics.RunPrometheusHttpServer()
	if err != nil {
		log.Panic(err)
	}
}
