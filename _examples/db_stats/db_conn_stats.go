package main

import (
	"context"
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/godevtools-pkg/metrics"
	_ "gitlab.com/godevtools-pkg/metrics"
	"log"
	"math/rand"
	"time"
)

func main() {
	fmt.Println("Gauge example")

	var db *sql.DB //implement here your own getting *sql.DB

	ticker := time.NewTicker(2 * time.Second)
	go func() {
		for {
			select {
			case <-ticker.C:
				for i := 0; i <= rand.Intn(10); i++ {
					go func() {
						sql := "SELECT * FROM users"
						db.Exec(sql)
					}()
				}
			}
		}
	}()

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	go metrics.ConnectionsCountTracking(ctx, db, 15*time.Second)

	if err := metrics.RunPrometheusHttpServer(); err != nil {
		log.Fatal(err.Error())
	}
}
