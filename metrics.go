package metrics

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rabbitmq/amqp091-go"
	"github.com/streadway/amqp"
	"net/http"
	"os"
	"strconv"
	"time"
)

var appPrefix, _ = os.LookupEnv("PROMETHEUS_APP_NAME")

var appRegistry *prometheus.Registry

// NumPings (simple counter): counts requests to ping-endpoint
var NumPings = promauto.NewCounter(prometheus.CounterOpts{
	Name: fmt.Sprintf("%s_pings_total", appPrefix),
	Help: "The total number of incoming ping requests",
})

// StatusCodeMetric (vector counter): counts requests(group by status code + method)
var StatusCodeMetric = promauto.NewCounterVec(
	prometheus.CounterOpts{
		Name: fmt.Sprintf("%s_status_code", appPrefix),
		Help: "HTTP response status code",
	},
	[]string{"code", "method"},
)

// RequestDurationMetric (histogram): calculates average request duration per path
var RequestDurationMetric = promauto.NewHistogramVec(prometheus.HistogramOpts{
	Name: fmt.Sprintf("%s_request_duration", appPrefix),
	Help: "HTTP request duration",
}, []string{"path"})

func StatusCodeMetricInc(statusCode int, method string) {
	StatusCodeMetric.With(prometheus.Labels{"code": strconv.Itoa(statusCode), "method": method}).Inc()
}

// CountDBConnectionsMetric (db_stats) number of open connections
var CountDBConnectionsMetric = promauto.NewGauge(
	prometheus.GaugeOpts{
		Namespace:   "database",
		Name:        fmt.Sprintf("%s_connections_open", appPrefix),
		Help:        "Number of database connections open.",
		ConstLabels: prometheus.Labels{"database": "relational"},
	})

var SizeQueueMetric = promauto.NewGaugeVec(prometheus.GaugeOpts{
	Name: fmt.Sprintf("%s_size_queue", appPrefix),
	Help: "Count of messages not awaiting acknowledgment",
}, []string{"queue"},
)

func init() {
	appRegistry = prometheus.NewRegistry()
	appRegistry.MustRegister(NumPings)
	appRegistry.MustRegister(StatusCodeMetric)
	appRegistry.MustRegister(RequestDurationMetric)
	appRegistry.MustRegister(CountDBConnectionsMetric)
	appRegistry.MustRegister(SizeQueueMetric)
}

func ConnectionsCountTracking(ctx context.Context, db *sql.DB, interval time.Duration) {
	if interval <= 10 {
		interval = 10 * time.Second
	}

	ticker := time.NewTicker(interval)

	for {
		select {
		case <-ctx.Done():
			return
		case <-ticker.C:
			CountDBConnectionsMetric.Set(float64(db.Stats().OpenConnections))
		}
	}
}

func QueueSizeTracking(ctx context.Context, queue string, amqpGoCh *amqp091.Channel, streadwayCh *amqp.Channel, interval time.Duration) error {

	if interval <= 10 {
		interval = 10 * time.Second
	}

	ticker := time.NewTicker(interval)

	if amqpGoCh != nil {
		for {
			select {
			case <-ctx.Done():
				return nil
			case <-ticker.C:
				q, err := amqpGoCh.QueueInspect(queue)
				if err != nil {
					return err
				}
				SizeQueueMetric.With(prometheus.Labels{"queue": queue}).Set(float64(q.Messages))
			}
		}
	} else if streadwayCh != nil {
		for {
			select {
			case <-ctx.Done():
				return nil
			case <-ticker.C:
				q, err := streadwayCh.QueueInspect(queue)
				if err != nil {
					return err
				}
				SizeQueueMetric.With(prometheus.Labels{"queue": queue}).Set(float64(q.Messages))
			}
		}
	} else {
		return fmt.Errorf("you need to specify a pointer to the message broker's open channel")
	}
}

func RunPrometheusHttpServer() error {
	http.Handle("/metrics", promhttp.Handler())
	return http.ListenAndServe(":8090", nil)
}
