package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"net/http"
)

// PrometheusResponseWriter implementation of ResponseWriter. It serves to access status code)
type PrometheusResponseWriter struct {
	http.ResponseWriter
	StatusCode int
}

func NewPrometheusResponseWriter(responseWriter http.ResponseWriter) *PrometheusResponseWriter {
	return &PrometheusResponseWriter{
		responseWriter,
		http.StatusOK,
	}
}

// WriteHeader adds status code value
func (pw *PrometheusResponseWriter) WriteHeader(statusCode int) {
	pw.StatusCode = statusCode
	pw.ResponseWriter.WriteHeader(statusCode)
}

// PrometheusMW middleware function. It serves to obtain some metrics.
func PrometheusMW(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {

		timer := prometheus.NewTimer(RequestDurationMetric.WithLabelValues(req.URL.Path))
		pw := NewPrometheusResponseWriter(w)
		next.ServeHTTP(pw, req)

		StatusCodeMetricInc(pw.StatusCode, req.Method)

		timer.ObserveDuration()
	})
}
