# Prometheus Metrics
## Install
`
    go get gitlab.com/godevtools-pkg/metrics
`

## How to use:
### Set environment variable: PROMETHEUS_APP_NAME
``
PROMETHEUS_APP_NAME=myapp
``

## Examples:
- Simple [here](./_examples/simple/simple.go)
- Middleware [here](./_examples/middleware/mw.go)
- Database connection statistics [here](./_examples/db_stats/db_conn_stats.go)
- Message broker queue size tracking [here](./_examples/rabbitmq_stats/queue_size_metric.go)

